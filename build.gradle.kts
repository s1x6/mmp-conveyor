import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    id("maven-publish")
    id("java-library")
}
group = "ru.nsu.team"
version = "1.0"

repositories {
    mavenCentral()
    jcenter()
}

publishing {
    publications {

        create<MavenPublication>("conveyor") {
            groupId = "ru.nsu.team"
            artifactId = "conveyor"
            version = "1.0"

            from(components["java"])
        }
    }
}


dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
}
tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}