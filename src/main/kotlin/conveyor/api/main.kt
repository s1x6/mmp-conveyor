package conveyor.api

import conveyor.api.conveyor.conveyor
import conveyor.api.node.Channel
import conveyor.api.node.node
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    val sumNode = node("sum") {
        input<Int>("1") { i ->
            println("Input ($name) got $i")
        }
        input<Int>("2") { i ->
            println("Input ($name) got $i")
        }

        output<String>("main") { s ->
            println("Output ($name) got $s")
        }

        onInputReady { parametersMap, outputChannelsMap ->
            val output = outputChannelsMap["main"] as Channel<String>
            val fParameter = parametersMap["1"]!!.asSingle<Int>()
            val sParameter = parametersMap["2"]!!.asSingle<Int>()
            output.put((fParameter + sParameter).toString())
        }
    }

    val printer = node("print") {
        input<String>("main") { i ->
            println("Input ($name) got $i")
        }

        onInputReady { parametersMap, _ ->
            val fParameter = parametersMap["main"]!!.asSingle<String>()
            println(fParameter)
        }
    }

    val c = conveyor {
        registerNode(sumNode)
        registerNode(printer)

        nodes("sum").outputs<String>("main") connectTo nodes("print").inputs("main")
    }

    c.start()
    GlobalScope.launch {
        sumNode.getInputChannels()["1"]?.put(1)
        delay(1000)
        sumNode.getInputChannels()["1"]?.put(2)
        delay(1000)
        sumNode.getInputChannels()["1"]?.put(3)
        delay(1000)
        sumNode.getInputChannels()["1"]?.put(4)
        delay(1000)
    }
    GlobalScope.launch {
        sumNode.getInputChannels()["2"]?.put(5)
        delay(500)
        sumNode.getInputChannels()["2"]?.put(6)
        delay(500)
        sumNode.getInputChannels()["2"]?.put(7)
        delay(500)
        sumNode.getInputChannels()["2"]?.put(8)
        delay(500)
    }
    Thread.sleep(6000)
    println("Put all args to sum node")


//
//    val lambda: Node.() -> Unit = {
//        input<String>("stringInput")
//        input<Int>("intInput") { i ->
//            println("I ($name) got $i")
//        }
//    }
//    val n1 = node(lambda)

    println(sumNode)
}