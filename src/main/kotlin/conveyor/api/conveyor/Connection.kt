package conveyor.api.conveyor

import conveyor.api.node.Channel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Connection<T>(private val firstChannel: Channel<T>, private val secondChannel: Channel<T>) {

    fun startTransition() {
        GlobalScope.launch {
            transit()
        }
    }

    private suspend fun transit() {
        while (true) {
            secondChannel.put(firstChannel.take())
        }
    }

}