package conveyor.api.conveyor

import conveyor.api.node.Node

class Conveyor {

    private val nodeMap: MutableMap<String, Node> = HashMap()
    private val connectionList: MutableList<Connection<*>> = ArrayList()
    private var isStarted = false
    private val inputNodes: MutableMap<String, Node> = HashMap()
    private val outputNodes: MutableMap<String, Node> = HashMap()

    fun addConnection(connection: Connection<*>) {
        connectionList.add(connection)
        if (isStarted) connection.startTransition()
    }

    fun setAsInputNode(nodeName: String) {
        inputNodes[nodeName] =
            nodeMap[nodeName] ?: throw IllegalArgumentException("Conveyor has no node with name: (${nodeName})")
    }

    fun setAsOutputNode(nodeName: String) {
        outputNodes[nodeName] =
            nodeMap[nodeName] ?: throw IllegalArgumentException("Conveyor has no node with name: (${nodeName})")
    }

    fun getNode(name: String): Node? {
        return nodeMap[name]
    }

    fun <T> putInputData(nodeName: String, channelName: String, obj: T) {
        inputNodes[nodeName]?.getInputChannels()?.get(channelName)?.put(obj as Any)
            ?: throw IllegalArgumentException("Cannot put data to node-(${nodeName}) input channel-(${channelName})")
    }

    suspend fun <T> getOutputData(nodeName: String, channelName: String): T {
        val obj = outputNodes[nodeName]?.getOutputChannels()?.get(channelName)?.take()
            ?: throw IllegalArgumentException("Cannot get data from node-(${nodeName}) input channel-(${channelName})")
        return obj as T
    }

    fun addNode(node: Node) {
        if (nodeMap.containsKey(node.name)) {
            throw RuntimeException("Node (${node.name}) already exists in conveyor")
        }
        nodeMap[node.name] = node
        if (isStarted) node.start()
    }

    fun start() {
//        return GlobalScope.launch {
        isStarted = true
        nodeMap.forEach { (_, v) -> v.start() }
        connectionList.forEach { it.startTransition() }
//        }
    }
}