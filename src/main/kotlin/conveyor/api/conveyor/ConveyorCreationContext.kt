package conveyor.api.conveyor

import conveyor.api.node.Channel
import conveyor.api.node.Node
import conveyor.api.node.NodeCreationContext

class ConveyorCreationContext {

    val nodeMap: MutableMap<String, Node> = HashMap()
    val connectionList: MutableList<Connection<Any>> = ArrayList()
    val inputNodesNames: MutableList<String> = ArrayList()
    val outputNodesNames: MutableList<String> = ArrayList()

    fun nodes(name: String): NodeHolder = NodeHolder(nodeMap[name]!!)

    fun registerNode(node: Node) {
        nodeMap[node.name] = node.copy()
    }

    fun registerAsInput(nodeName: String){
        inputNodesNames.add(nodeName)
    }

    fun registerAsOutput(nodeName: String){
        outputNodesNames.add(nodeName)
    }

    fun node(name: String, initLambda: NodeCreationContext.() -> Unit) {
        val context = NodeCreationContext()
        initLambda(context)                   // initLambda.invoke(node)
        val node = Node(name, context.onInputReadyAction)
        context.inputMap.values.forEach(node::addInputChannel)
        context.outputMap.values.forEach(node::addOutputChannel)
        nodeMap[name] = node
    }

    infix fun <T> Channel<T>.connectTo(channel: Channel<T>) {
        val connection = Connection(this, channel)
        connectionList.add(connection as Connection<Any>)
    }

}

class NodeHolder(private val node: Node) {

    fun <T> inputs(name: String): Channel<T> = (node.getInputChannels()[name] as Channel<T>?)!!

    fun <T> outputs(name: String): Channel<T> = (node.getOutputChannels()[name] as Channel<T>?)!!

}

fun conveyor(initLambda: ConveyorCreationContext.() -> Unit): Conveyor {
    val c = ConveyorCreationContext()
    initLambda(c)
    val conveyor = Conveyor()
    c.nodeMap.values.forEach(conveyor::addNode)
    c.connectionList.forEach(conveyor::addConnection)
    c.inputNodesNames.forEach(conveyor::setAsInputNode)
    c.outputNodesNames.forEach(conveyor::setAsOutputNode)
    return conveyor
}