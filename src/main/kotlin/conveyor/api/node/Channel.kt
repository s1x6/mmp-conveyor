package conveyor.api.node

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Channel<T>(val awaitingLimit: Int) {
    private val channel: kotlinx.coroutines.channels.Channel<T> = kotlinx.coroutines.channels.Channel()
    var name: String = "defaultName"
    lateinit var onPutAction: (Channel<T>.(T) -> Unit)

    fun put(obj: T) {
        // оставил функцию не suspended чтобы ее можно было вызывать не из корутин
        // иначе не получается положить объект в ноде
        GlobalScope.launch {
            onPutAction.invoke(this@Channel, obj)
            channel.send(obj)
        }
    }

    suspend fun take(): T {
        return channel.receive()
    }

    fun copy(): Channel<T> {
        return Channel<T>(this.awaitingLimit).apply { name = this@Channel.name
        onPutAction = this@Channel.onPutAction}
    }
}