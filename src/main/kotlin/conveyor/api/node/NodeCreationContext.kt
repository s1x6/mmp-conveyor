package conveyor.api.node

class NodeCreationContext {

    val inputMap: MutableMap<String, Channel<Any>> = HashMap()
    val outputMap: MutableMap<String, Channel<Any>> = HashMap()
    var onInputReadyAction: Node.(Map<String, Node.ParamsList>, Map<String, Channel<Any>>) -> Unit = { _, _ -> }

    fun <T> input(name: String, awaitingLimit: Int = 1, onPut: Channel<T>.(T) -> Unit = {}) {
        val channel = Channel<T>(awaitingLimit)
        channel.onPutAction = onPut
        channel.name = name
        inputMap[name] = channel as Channel<Any>
    }

    fun <T> output(name: String, onPut: Channel<T>.(T) -> Unit = {}) {
        val channel = Channel<T>(1)
        channel.onPutAction = onPut
        channel.name = name
        outputMap[name] = channel as Channel<Any>
    }

    fun onInputReady(action: Node.(Map<String, Node.ParamsList>, Map<String, Channel<*>>) -> Unit = { _, _ -> }) {
        onInputReadyAction = action
    }
}


fun node(name: String, initLambda: NodeCreationContext.() -> Unit): Node {
    val context = NodeCreationContext()
    initLambda(context)                   // initLambda.invoke(node)
    val node = Node(name, context.onInputReadyAction)
    context.inputMap.values.forEach(node::addInputChannel)
    context.outputMap.values.forEach(node::addOutputChannel)
    return node
}