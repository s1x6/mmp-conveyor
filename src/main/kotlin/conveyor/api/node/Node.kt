package conveyor.api.node

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Node(
    val name: String,
    action: Node.(Map<String, ParamsList>, Map<String, Channel<Any>>) -> Unit
) {

    var action = action
        private set
    private val inputMap: MutableMap<String, Channel<Any>> = HashMap()
    private val outputMap: MutableMap<String, Channel<Any>> = HashMap()

    fun addInputChannel(inputChannel: Channel<Any>) {
        if (inputMap.containsKey(inputChannel.name)) {
            throw RuntimeException("This input (${inputChannel.name}) already exists in node ($name)")
        }
        inputMap[inputChannel.name] = inputChannel
    }

    fun addOutputChannel(outputChannel: Channel<Any>) {
        if (outputMap.containsKey(outputChannel.name)) {
            throw RuntimeException("This output (${outputChannel.name}) already exists in node ($name)")
        }
        outputMap[outputChannel.name] = outputChannel
    }

    fun getInputChannels(): Map<String, Channel<Any>> = inputMap
    fun getOutputChannels(): Map<String, Channel<Any>> = outputMap

    fun start() {
        GlobalScope.launch(CoroutineName("myСoroutine")) {
            waitForInputAndProcess()
        }
    }

    private suspend fun waitForInputAndProcess() {
        while (true) {
            val map = HashMap<String, ParamsList>()
            inputMap.forEach { (k, v) ->
                val list = paramListOf()
                repeat(v.awaitingLimit) {
                    list.add(v.take())
                }
                map[k] = list
            }
            action(map, outputMap)
        }
    }

    class ParamsList {
        private val list = mutableListOf<Any>()

        fun add(obj: Any) = list.add(obj)

        operator fun get(num: Int): Any {
            return list[num]
        }

        fun <T> asSingle(): T = list[0] as T
        fun <T> asMany(): List<T> = list as List<T>

    }

    private fun paramListOf() = ParamsList()

    fun copy(): Node{
        val newNode = Node(this.name, this.action)
        inputMap.values.forEach { v ->
            newNode.addInputChannel(v.copy())
        }
        outputMap.values.forEach { v ->
            newNode.addOutputChannel(v.copy())
        }
        return newNode
    }
}
