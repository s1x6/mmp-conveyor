package conveyor.api.model

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

class ImageArchive {
    private val list = mutableListOf<Image>()

    fun addCompressed(image: Image) {
        runBlocking {
            // emulate compressing
            delay(1000)
            image.applyFilter("compressed")
            list.add(image)
        }
    }

    fun getImages(): List<Image> = list
}