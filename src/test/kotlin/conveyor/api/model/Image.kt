package conveyor.api.model

interface Image {
    //public var data = "original"

    abstract fun print()

    fun applyFilter(filterName: String): Image

    fun getBytes(): ByteArray

    //fun addImages (vararg input: Image) : Image

    abstract fun separateImage(num: Int): List<Image>
}