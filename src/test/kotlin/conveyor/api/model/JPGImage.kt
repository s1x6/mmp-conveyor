package conveyor.api.model

class JPGImage(var data: String = "original") : Image {
    override fun print() {
        println("jpg image: \n$data")
    }

    override fun applyFilter(filterName: String): JPGImage {
        data += " $filterName"
        return this
    }

    override fun getBytes() = "jpg image: \n$data".toByteArray()


    fun addImages(vararg input: JPGImage): JPGImage {
        for (item in input) {
            data += "\n" + item.data
        }
        return this
    }

    override fun separateImage(num: Int): List<JPGImage> {
        val images : MutableList<JPGImage> = mutableListOf()
        for (i in 1..num) {
            images.add(JPGImage(data).applyFilter("/$i part"))
        }
        return images;
    }
}