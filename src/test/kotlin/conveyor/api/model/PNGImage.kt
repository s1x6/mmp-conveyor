package conveyor.api.model

class PNGImage (var data: String = "original") : Image {
    override fun print() {
        println("png image: \n$data")
    }

    override fun applyFilter(filterName: String): PNGImage {
        data += " $filterName"
        return this
    }

    fun addImages(vararg input: PNGImage): PNGImage {
        for (item in input) {
            data += "\n" + item.data
        }
        return this
    }

    override fun getBytes() = "png image: \n$data".toByteArray()


    override fun separateImage(num: Int): List<PNGImage> {
        val images: MutableList<PNGImage> = mutableListOf()
        for (i in 1..num) {
            images.add(PNGImage(data).applyFilter(" /$i part"))
        }
        return images;
    }
}