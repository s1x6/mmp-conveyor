package conveyor.api.model

//class SVGImage (d: String = "original") : Image(data = d) {
class SVGImage(var data: String = "original") : Image {
    override fun print() {
        println("svg image: \n$data")
    }

    override fun applyFilter(filterName: String): SVGImage {
        data += " $filterName"
        return this
    }

    override fun getBytes() = "svg image: \n$data".toByteArray()

    fun addImages(vararg input: SVGImage): SVGImage {
        for (item in input) {
            data += "\n" + item.data
        }
        return this
    }

    override fun separateImage(num: Int): List<SVGImage> {
        val images: MutableList<SVGImage> = mutableListOf()
        for (i in 1..num) {
            images.add(SVGImage(data).applyFilter("/$i part"))
        }
        return images;
    }
}