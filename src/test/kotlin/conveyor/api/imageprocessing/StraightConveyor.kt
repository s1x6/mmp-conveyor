package conveyor.api.imageprocessing

import conveyor.api.conveyor.Connection
import conveyor.api.conveyor.conveyor
import conveyor.api.model.JPGImage
import conveyor.api.model.PNGImage
import conveyor.api.node.Channel
import conveyor.api.node.node
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StraightConveyor {

    @Test
    fun testStraightConveyor() = runBlocking {
        val grayScale = getGrayScaleNode()
        val toRGBNode = getRGBNode()
        val blurNode = getBlurNode()

        val c = conveyor {
            registerNode(blurNode)
            registerNode(grayScale)
            registerNode(getJpgToPngNode())
            registerNode(getPngToJpgNode())
            registerNode(toRGBNode)
            registerNode(getPanoramaNode())
            registerNode(getPrinter())

            registerAsInput("gray scale")

            registerAsOutput("separate to rgb")
            registerAsOutput("blur")

            nodes("gray scale").outputs<PNGImage>("1") connectTo nodes("png to jpg").inputs("1")
            nodes("png to jpg").outputs<PNGImage>("1") connectTo nodes("separate to rgb").inputs("1")
            //nodes("separate to rgb").outputs<PNGImage>("red") connectTo nodes("print").inputs("print")
            //nodes("separate to rgb").outputs<PNGImage>("green") connectTo nodes("print").inputs("print")
            nodes("separate to rgb").outputs<PNGImage>("blue") connectTo nodes("blur").inputs("1")
            //nodes("blur").outputs<PNGImage>("1") connectTo nodes("print").inputs("print")
        }

        c.start()

        // меняем запущенный конвейер
        c.addNode(node("newFilter") {
            input<PNGImage>("1")
            output<PNGImage>("main") { i ->
                println("I made this -> ${String(i.getBytes())}")
            }

            onInputReady { a, b ->
                val image = a["1"]?.asSingle<PNGImage>()!!
                image.applyFilter("median")
                (b["main"] as Channel<PNGImage>).put(image)
            }
        })
        c.setAsInputNode("newFilter")
        c.addConnection(
            Connection(
                c.getNode("newFilter")?.getOutputChannels()?.get("main")!!,
                c.getNode("png to jpg")?.getInputChannels()?.get("1")!!
            )
        )

        c.putInputData("gray scale", "1", PNGImage())
        c.putInputData("newFilter", "1", PNGImage())
        Thread.sleep(1000)
        assertEquals("original /gray scale /red", c.getOutputData<JPGImage>("separate to rgb", "red").data)
        assertEquals("original /gray scale /green", c.getOutputData<JPGImage>("separate to rgb", "green").data)
        assertEquals("original /gray scale /blue /blur", c.getOutputData<JPGImage>("blur", "1").data)
        Thread.sleep(6000)
        println("Put all args to node")
    }
    
}


