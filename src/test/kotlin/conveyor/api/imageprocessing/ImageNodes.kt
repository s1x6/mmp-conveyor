package conveyor.api.imageprocessing

import conveyor.api.model.Image
import conveyor.api.model.ImageArchive
import conveyor.api.model.JPGImage
import conveyor.api.model.PNGImage
import conveyor.api.node.Channel
import conveyor.api.node.node

fun getBlurNode() = node("blur") {
    input<JPGImage>("1") { i ->
        println("Input ($name) got $i")
    }
    output<JPGImage>("1")
    onInputReady { paramListMap, outputChannelsMap ->
        val parameter = paramListMap["1"]!!.asSingle<JPGImage>()
        (outputChannelsMap["1"] as Channel<JPGImage>?)?.put(parameter.applyFilter("/blur"))
    }
}

fun getMedianNode() = node("median") {
    input<JPGImage>("1")
    output<JPGImage>("1")
    onInputReady { paramListMap, outputChannelsMap ->
        val parameter = paramListMap["1"]!!.asSingle<JPGImage>()
        (outputChannelsMap["1"] as Channel<JPGImage>?)?.put(parameter.applyFilter("/median"))
    }
}

fun getJpgToPngNode() = node("jpg to png") {
    input<JPGImage>("1")
    output<PNGImage>("1")
    onInputReady { paramListMap, outputChannelsMap ->
        val parameter = paramListMap["1"]!!.asSingle<JPGImage>()
        (outputChannelsMap["1"] as Channel<PNGImage>?)?.put(PNGImage(parameter.data))
    }
}

fun getPngToJpgNode() = node("png to jpg") {
    input<PNGImage>("1")
    output<JPGImage>("1")
    onInputReady { paramListMap, outputChannelsMap ->
        val parameter = paramListMap["1"]!!.asSingle<PNGImage>()
        (outputChannelsMap["1"] as Channel<JPGImage>?)?.put(JPGImage(parameter.data))
    }
}

fun getGrayScaleNode() = node("gray scale") {
    input<PNGImage>("1")
    output<PNGImage>("1")
    onInputReady { paramListMap, outputChannelsMap ->
        val parameter = paramListMap["1"]!!.asSingle<PNGImage>()
        (outputChannelsMap["1"] as Channel<PNGImage>?)?.put(parameter.applyFilter("/gray scale"))
    }
}

fun getRGBNode() = node("separate to rgb") {
    input<JPGImage>("1")
    output<JPGImage>("red")
    output<JPGImage>("green")
    output<JPGImage>("blue")
    onInputReady { paramListMap, outputChannelsMap ->
        val parameter = paramListMap["1"]!!.asSingle<JPGImage>()
        (outputChannelsMap["red"] as Channel<JPGImage>?)?.put(JPGImage(parameter.data).applyFilter("/red"))
        (outputChannelsMap["green"] as Channel<JPGImage>?)?.put(JPGImage(parameter.data).applyFilter("/green"))
        (outputChannelsMap["blue"] as Channel<JPGImage>?)?.put(JPGImage(parameter.data).applyFilter("/blue"))
    }
}

fun getPanoramaNode() = node("create panorama") {
    input<JPGImage>("1")
    input<JPGImage>("2")
    input<JPGImage>("3")
    input<JPGImage>("4")
    output<JPGImage>("1")
    onInputReady { paramListMap, outputChannelsMap ->
        val first = paramListMap["1"]!!.asSingle<JPGImage>()
        val second = paramListMap["2"]!!.asSingle<JPGImage>()
        val third = paramListMap["3"]!!.asSingle<JPGImage>()
        val fourth = paramListMap["4"]!!.asSingle<JPGImage>()
        first.addImages(second, third, fourth)
        (outputChannelsMap["1"] as Channel<JPGImage>?)?.put(first)
    }
}

fun getPrinter() = node("print") {
    input<Image>("print")

    onInputReady { paramListMap, _ ->
        val parameter = paramListMap["print"]!!.asSingle<Image>()
        parameter.print()
    }

}

fun getPackerNode() = node("packer") {
    input<Image>("images", 3)
    input<ImageArchive>("archive")

    onInputReady { paramListMap, outMap ->
        val archive = paramListMap["archive"]!!.asSingle<ImageArchive>()
        paramListMap["images"]!!.asMany<Image>().forEach(archive::addCompressed)
        (outMap["out"] as Channel<ImageArchive>).put(archive)
    }

    output<ImageArchive>("out")
}

fun getPackerPrinterNode() = node("printPack") {
    input<ImageArchive>("1")

    onInputReady { paramListMap, _ ->
        val parameter = paramListMap["1"]!!.asSingle<ImageArchive>()
        println(
            """
archive contains {
${
                parameter.getImages().joinToString(separator = System.lineSeparator()) {
                    "\t ${
                        String(it.getBytes()).replace(
                            "\n",
                            " "
                        )
                    }"
                }
            }
}
        """.trimIndent()
        )
    }
}

fun getBlurCheckerNode(blurTimesRequired: Int) = node("check") {
    input<Image>("1")
    output<Image>("enough")
    output<Image>("not-enough")

    onInputReady { paramsMap, outputMap ->
        val image = paramsMap["1"]!!.asSingle<Image>()
        val outKey = if (String(image.getBytes()).split("blur").size > blurTimesRequired) "enough" else "not-enough"
        (outputMap[outKey] as Channel<Image>).put(image)
    }
}