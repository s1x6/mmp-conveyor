package conveyor.api.imageprocessing

import conveyor.api.conveyor.conveyor
import conveyor.api.model.Image
import conveyor.api.model.PNGImage
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

class CycleTest {

    @Test
    fun cycleTest() {
        val pngToJpg = getPngToJpgNode()
        val c = conveyor {
            registerNode(pngToJpg)
            registerNode(getBlurNode())
            registerNode(getBlurCheckerNode(7))
            registerNode(getPrinter())

            registerAsInput("png to jpg")

            nodes("png to jpg").outputs<Image>("1") connectTo nodes("blur").inputs("1")
            nodes("blur").outputs<Image>("1") connectTo nodes("check").inputs("1")
            nodes("check").outputs<Image>("enough") connectTo nodes("print").inputs("print")
            nodes("check").outputs<Image>("not-enough") connectTo nodes("blur").inputs("1")
        }
        c.start()

        c.putInputData("png to jpg", "1", PNGImage())
        runBlocking { delay(2000) }
    }
}