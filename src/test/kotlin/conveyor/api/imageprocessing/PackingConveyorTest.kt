package conveyor.api.imageprocessing

import conveyor.api.conveyor.conveyor
import conveyor.api.model.ImageArchive
import conveyor.api.model.PNGImage
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

class PackingConveyorTest {

    @Test
    fun testPacker() {
        val packerNode = getPackerNode()
        val c = conveyor {
            registerNode(packerNode)
            registerNode(getPackerPrinterNode())

            registerAsInput(packerNode.name)

            nodes("packer").outputs<ImageArchive>("out") connectTo nodes("printPack").inputs("1")
        }
        c.start()
        runBlocking {
            repeat(9) {
                c.putInputData("packer", "images", PNGImage(it.toString()))
            }
            repeat(3) {
                c.putInputData("packer", "archive", ImageArchive())
            }

            delay(10000)
        }
    }
}