package conveyor.api.imageprocessing

import conveyor.api.conveyor.conveyor
import conveyor.api.model.JPGImage
import conveyor.api.model.PNGImage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DoubleUseOfNodes {
    @Test
    fun testStraightConveyor() = runBlocking {
        val grayScale = getGrayScaleNode()
        val toRGBNode = getRGBNode()
        val blurNode = getBlurNode()

        val c1 = conveyor {
            registerNode(blurNode)
            registerNode(grayScale)
            registerNode(getPngToJpgNode())
            registerNode(toRGBNode)
            registerNode(getPrinter())

            registerAsInput("gray scale")

            registerAsOutput("separate to rgb")
            registerAsOutput("blur")

            nodes("gray scale").outputs<PNGImage>("1") connectTo nodes("png to jpg").inputs("1")
            nodes("png to jpg").outputs<PNGImage>("1") connectTo nodes("separate to rgb").inputs("1")
            nodes("separate to rgb").outputs<PNGImage>("red") connectTo nodes("print").inputs("print")
            nodes("separate to rgb").outputs<PNGImage>("green") connectTo nodes("print").inputs("print")
            nodes("separate to rgb").outputs<PNGImage>("blue") connectTo nodes("blur").inputs("1")
            nodes("blur").outputs<PNGImage>("1") connectTo nodes("print").inputs("print")
        }

        val c2 = conveyor {
            registerNode(getMedianNode())
            registerNode(getPngToJpgNode())
            registerNode(toRGBNode)
            registerNode(getPrinter())
            registerNode(blurNode)

            registerAsInput("median")

            registerAsOutput("separate to rgb")
            registerAsOutput("blur")

            nodes("median").outputs<PNGImage>("1") connectTo nodes("separate to rgb").inputs("1")
            //nodes("png to jpg").outputs<PNGImage>("1") connectTo nodes("separate to rgb").inputs("1")
            //nodes("separate to rgb").outputs<PNGImage>("red") connectTo nodes("print").inputs("print")
            //nodes("separate to rgb").outputs<PNGImage>("green") connectTo nodes("print").inputs("print")
            nodes("separate to rgb").outputs<PNGImage>("blue") connectTo nodes("blur").inputs("1")
            //nodes("blur").outputs<PNGImage>("1") connectTo nodes("print").inputs("print")
        }

        c1.start()
        c2.start()
        grayScale.getInputChannels()["1"]?.put(PNGImage())
        Thread.sleep(1000)

        GlobalScope.launch {
            c1.putInputData("blur", "1", JPGImage())
            delay(1000)
            c1.putInputData("blur", "1", JPGImage())
            delay(1000)
        }
        GlobalScope.launch {
            c1.putInputData("gray scale", "1", PNGImage())
            delay(3000)
            c1.putInputData("gray scale", "1", PNGImage())
            delay(3000)
        }
        GlobalScope.launch {
            c2.putInputData("median", "1", JPGImage())
            delay(500)
            c2.putInputData("median", "1", JPGImage())
            delay(500)
        }
        GlobalScope.launch {
            c2.putInputData("gray scale", "1", PNGImage())
            delay(2000)
            c2.putInputData("gray scale", "1", PNGImage())
            delay(2000)
        }
        GlobalScope.launch {
            for(i in 1..2){
                println("First conv: ${c1.getOutputData<JPGImage>("separate to rgb", "red").data}")
                println("First conv: ${c1.getOutputData<JPGImage>("separate to rgb", "green").data}")
                println("First conv: ${c1.getOutputData<JPGImage>("blur", "1").data}")
            }
        }
        GlobalScope.launch {
            for(i in 1..2){
                println("First conv: ${c2.getOutputData<JPGImage>("separate to rgb", "red").data}")
                println("First conv: ${c2.getOutputData<JPGImage>("separate to rgb", "green").data}")
                println("First conv: ${c2.getOutputData<JPGImage>("blur", "1").data}")
            }
        }
        println("Put all args to node")
    }
}