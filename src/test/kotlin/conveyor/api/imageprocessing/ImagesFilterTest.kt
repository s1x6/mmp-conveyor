package conveyor.api.imageprocessing

import conveyor.api.conveyor.conveyor
import conveyor.api.model.JPGImage
import conveyor.api.model.PNGImage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.junit.jupiter.api.Test

class ImagesFilterTest {

    @Test
    fun filterTest() {
        val blurNode = getBlurNode()
        val jpgToPng = getJpgToPngNode()
        val pngToJpg = getPngToJpgNode()
        val grayScale = getGrayScaleNode()
        val toRGBNode = getRGBNode()
        val createPanorama = getPanoramaNode()
        val printer = getPrinter()

        val c = conveyor {
            registerNode(blurNode)
            registerNode(grayScale)
            registerNode(jpgToPng)
            registerNode(pngToJpg)
            registerNode(toRGBNode)
            registerNode(createPanorama)
            registerNode(printer)

            registerAsInput(blurNode.name)
            registerAsInput(grayScale.name)

            nodes("blur").outputs<JPGImage>("1") connectTo nodes("separate to rgb").inputs("1")
            nodes("separate to rgb").outputs<JPGImage>("red") connectTo nodes("create panorama").inputs("1")
            nodes("separate to rgb").outputs<JPGImage>("green") connectTo nodes("create panorama").inputs("2")
            nodes("separate to rgb").outputs<JPGImage>("blue") connectTo nodes("create panorama").inputs("3")

            nodes("gray scale").outputs<PNGImage>("1") connectTo nodes("png to jpg").inputs("1")
            nodes("png to jpg").outputs<JPGImage>("1") connectTo nodes("create panorama").inputs("4")

            nodes("create panorama").outputs<JPGImage>("1") connectTo nodes("print").inputs("print")
        }

        c.start()
        GlobalScope.launch {
            c.putInputData("blur", "1", JPGImage())
            delay(1000)
            c.putInputData("blur", "1", JPGImage())
            delay(1000)
            c.putInputData("blur", "1", JPGImage())
            delay(1000)
        }
        GlobalScope.launch {
            c.putInputData("gray scale", "1", PNGImage())
            delay(500)
            c.putInputData("gray scale", "1", PNGImage())
            delay(500)
            c.putInputData("gray scale", "1", PNGImage())
            delay(500)
        }
        Thread.sleep(6000)
        println("Put all args to sum node")

        //println(sumNode)
    }
}