# Conveyor
## Назначение
Данная библиотека позволяет производить конвейерные вычисления или преобразования. С помощью удобного DSL опишите архитектуру вычислительных узлов и связи между ними, а остальное сделает за вас библиотека!
## Getting started
Склонируйте репозиторий  
``git clone https://gitlab.com/s1x6/mmp-conveyor.git``  

Запуск тестов ``./gradlew test``  

В случае, если необходимо использовать библиотеку в стороннем проекте, то опубликуйте ее в локальный репозиторий, вызвав ``./gradlew publishConveyorPublicationToMavenLocal``  
После этого, в нужном проекте подключите локальный репозиторий
```kotlin
repositories {  
      mavenLocal()
  }
```
И укажите конкретную зависимость ``implementation("ru.nsu.team:conveyor:1.0")
``
## API
### Node
Node - это вычислительный узел. Он может иметь несколько входов, несколько выходов и он выполняет назначенные ему вычисления с объектами, подаваемыми на вход.
Для того, чтобы создать новую ноду, используется следующий синтаксис:
```kotlin
val n = node("sum") { // имя узла
        
    }
```
Но данный узел не имеет ни входов, ни выходов, и ничего не делает. Добавим некоторую логику:
```kotlin
val n = node("sum") { // имя узла
        input<Int>("1") // вход 1
        input<Int>("2") // вход 2

        output<String>("main") // выход с именем main

        onInputReady { parametersMap, outputChannelsMap -> // основная логика узлв
            val output = outputChannelsMap["main"] as Channel<String>
            val fParameter = parametersMap["1"]!!.asSingle<Int>()
            val sParameter = parametersMap["2"]!!.asSingle<Int>()
            output.put((fParameter + sParameter).toString())
        }
    }
```
Что здесь происходит: мы объявили, что в нашем узле есть два входа и дали им имена, чтобы на них можно было сослаться в будущем.
Также указали и выход. Все входные и выходные каналы типизированы и не получится подать на вход объект случайного типа.  
В ``onInputReady`` на вход подается два аргумента 
- ``parametersMap: Map<String, Node.ParamsList>`` - позволяет получить список аргументов по имени входного канала
- ``outputChannelsMap: Map<String, Channel<Any>>`` - позволяет получить выходной канал, в который можно положить результат вычислений 

Методы ``asSingle<T>`` и ``asMany<T>`` позволяют получить типизированный аргумент (если точно ждем один) либо список аргументов (если ждем несколько)

Как же все это работает: каждый раз, когда из каждого входного канала было получено необходимое количество объектов, вызывается описанная в ``onInputReady`` лямбда  
По умолчанию из каждого входного канала ожидается 1 объект, но можно указать любое количество, например
```kotlin
node {
    input<Int>("1", 5) // из этого канала будет ожидаться 5 объектов для запуска вычислительной логики узла
}
```

### Логи
Создаваемые в узле каналы также могут вызывать различные сайд-эффекты при попадании в них новых объектов
Например, можно логгировать получаемые на вход числа:
```kotlin
val n = node("sum") { // имя узла
        input<Int>("1") { i -> // полученный Int
            println("Input ($name) got $i") // выводим в консоль
        }
        input<Int>("2") { i ->
            println("Input ($name) got $i")
        }

        output<String>("main") { s ->
            println("Output ($name) got $s")
        }

        onInputReady { parametersMap, outputChannelsMap ->
            val output = outputChannelsMap["main"] as Channel<String>
            val fParameter = parametersMap["1"]!!.asSingle<Int>()
            val sParameter = parametersMap["2"]!!.asSingle<Int>()
            output.put((fParameter + sParameter).toString())
        }
    }
```

## Conveyor
Много ли можно навычислять с одной нодой? Для серьезных вычислений необходимо собрать несколько нод в конвейер!
```kotlin
val c = conveyor {
        registerNode(sumNode) // добавить существующую ноду
        node("print") { // добавить анонимно созданную ноду
                input<String>("main") 
        
                onInputReady { parametersMap, _ ->
                    val fParameter = parametersMap["main"]!!.asSingle<String>()
                    println(fParameter)
                }
            }
    }
```

Отлично, теперь есть две ноды в одно конвейере. Осталось задать их связь, для этого есть оператор ``connectTo``
```kotlin
val c = conveyor {
        {...}
        nodes("sum").outputs<String>("main") connectTo nodes("print").inputs("main")
    }
```
Теперь каждый раз при суммировании двух чисел, результирующая строка будет отправляться на вход печатающей ноды.

Важным этапом является указание, какие именно годы являются входными для конвейера, а какие выходными. То есть куда нам подавать объекты в конвейер и откуда их ждать на выходе
Для этого есть отдельные функции
```kotlin
val n = node { ... }
val c = conveyor {
        node("sum") { ... }
        registerNode(n)
        setAsInputNode("sum")    
        setAsOutputNode(n.name)
    }
```
После этого необходимо запустить работу конвейера и можно проверить его работу.
```kotlin
val c = conveyor { ... }
c.start()
c.putInputData<Int>("nodeName", "channelName", 5)
runBlocking {
    val result = c.getOutputData<Int>("anotherNodeName", "nodeChannel")
}
```
Да, конвейеры работают на корутинах, поэтому необходимо ожидать результат работы из ``CoroutineScope``, так как данный метод является ``suspended``

Вам также не придется писать одни и те же ноды для использования их в разных контейнерах, достаточно один раз положить ноду в переменную и затем добавить ее во все необходимые контейнеры. Все будет работать корректно
```kotlin
val n = node { ... }
val c1 = conveyor {
    registerNode(n)
    ...
}
val c2 = conveyor {
    registerNode(n)
    ...
}
c1.start()
c2.start()
```

### Циклы
Можно также создать конвейер, в котором объекты в како-то части их обработки будут ходить в цикле
```kotlin
val pngToJpg = getPngToJpgNode()
        val c = conveyor {
            registerNode(pngToJpg)
            registerNode(getBlurNode())
            registerNode(getBlurCheckerNode(5)) // пропустит на выход только если фильтр Blur был применен как минимум 5 раз
            registerNode(getPrinter())

            ...
            // обратите внимание, что в одному входу подсоединено несколько выходов
            nodes("png to jpg").outputs<Image>("1") connectTo nodes("blur").inputs("1") // здесь
            nodes("blur").outputs<Image>("1") connectTo nodes("check").inputs("1")
            nodes("check").outputs<Image>("enough") connectTo nodes("print").inputs("print")
            nodes("check").outputs<Image>("not-enough") connectTo nodes("blur").inputs("1") // и здесь
        }
```